// Project: TCP Server Implementation In C++.
// Architects: Mrinal Wahal & Ashwin Gupta.
// Purpose: Semester 3 Open-Ended Project.

// Required Notice:
      // Requires MinGW Compiler on Windows and GPP on *nix.
      // Add '-lws2_32' in terminal arguments while compiling.


#define _WIN32_WINNT 0x501
#include<iostream>
#include<WS2tcpip.h>
#include<winsock2.h>
#include<chrono>
#include<ctime>
#pragma comment (lib, "ws2_32.lib")

using namespace std;

int main() {

    //cout << std::chrono::system_clock::now();
    // Initialize WinSocket.
    WSADATA wsdata;
    WORD ver = MAKEWORD(1, 0);

    // WSAStartup() initialized WinSocket for DLL.
    int ws_fine = WSAStartup(ver, &wsdata);

    if (ws_fine != 0) {
      cerr << "[-] Could not initialize Winsock."<<endl;
    }
    else {
      cout << "[+] Winsock succesfully initialized."<<endl;
    }


    // Create a Socket Object.
    SOCKET host_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (host_socket == INVALID_SOCKET) {
        cerr << "[-] Socket could not be initialized." << endl;
    }
    else {
      cout << "[+] Socket succesfully initialized." << endl;
    }

    // Bind the Socket to IP/Port.
    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(4000); //HTONS = Host To Network Short.
    addr.sin_addr.S_un.S_addr = INADDR_ANY; //Can also use inet_pton.

    bind(host_socket, (sockaddr*)&addr, sizeof(addr));
    if (bind == 0) {
        cerr << "[-] Socket could not be binded." << endl;
    }
    else {
      cout << "[+] Socket succesfully binded to " << addr.sin_addr.S_un.S_addr;
      cout << "at port " << addr.sin_port << endl;
    }

    // Set Socket to Listening Mode.
    listen (host_socket, SOMAXCONN);
    cout << "[+] Socket is listening for incoming connections." << endl;

    // Wait for a Connection & Accept.

    sockaddr_in client;
    int clientSize = sizeof(client);
    SOCKET client_socket = accept(host_socket, (sockaddr*)&client_socket, &clientSize);

    char client_name[NI_MAXHOST];
    char client_port[NI_MAXHOST];

    ZeroMemory(client_name, NI_MAXHOST);
    ZeroMemory(client_port, NI_MAXHOST);

    //if (getnameinfo((sockaddr*)&client, sizeof(client), client_name, NI_MAXHOST, client_port, NI_MAXHOST, 0) == 0)
    //{
      cout << "[+] " << client_name << " connected on " << client_port << endl;
    /*}
    else if ()
    else {
      inet_ntop(AF_INET, &client.sin_addr, client_name,NI_MAXHOST);
      cout << "[+] " << client_name << "connected on " << ntohs(client.sin_port) << endl;
    }
    */

    //Close the Host Socket.
    //closesocket(host_socket);

    // Loop: Recv and Echo the messages.
    char buffer[4096];

    while(1) {

        ZeroMemory(buffer, 4096);

        int recieved_msg = recv(client_socket, buffer, 4096, 0);
        if (recieved_msg == SOCKET_ERROR) {
            cerr << "[-] Error in Recieving Messages." << endl;
        }
        else if (recieved_msg == 0) {
            cout << "[-] Client seems to be lazy. It's not responding." << endl;
            break;
        }
        else {
          break;
        }

        // Echo the message back.
        send(client_socket, buffer, recieved_msg + 1, 0);
        // An extra byte is sent with the message in order to avoid 0 buffer problems.
    }
    // Close the client socket.
    closesocket(client_socket);

    // Cleanup WinSock.
    WSACleanup();

    return 0;
}
